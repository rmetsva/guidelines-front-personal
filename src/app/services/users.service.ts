import { Injectable } from '@angular/core';
import { HttpModule, Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class UsersService {

  private url: string;
  headers;
  options;

  constructor(private http: Http) {
    this.url = 'http://localhost:8080/';
    this.headers = new Headers({ 'Content-Type': 'application/json' });
    this.options = new RequestOptions({ headers: this.headers });
   }

  getUsers(): Observable<any[]> {
    return this.http.get(this.url + 'userlist')
                    .map(data => data.json());
  }

  deleteUser(i: string): Observable<any> {
    return this.http.delete(this.url + 'delete-user?userName=' + i);
  }

  addUser(name, email, birthday): Observable<any> {
    return this.http.post(this.url + 'create-user?userName=' + name + '&userEmail=' + email + '&userBirthday=' + birthday, this.options);
  }

  getGroups(): Observable<any[]> {
    return this.http.get(this.url + 'grouplist')
                    .map(data => data.json());
  }

  getUsersForGrouping(group: string): Observable<any[]> {
    return this.http.get(this.url + 'userlistforgroup?groupingName=' + group)
                    .map(data => data.json());
  }

  addGroup(groupName: string): Observable<any> {
      return this.http.post(this.url + 'create-group?groupingName=' + groupName, this.options);
  }

  deleteGroup(groupName: string): Observable<any> {
    return this.http.delete(this.url + 'delete-group?groupingName=' + groupName);
  }

  editUser(oldName:string, newName:string, newEmail:string): Observable<any> {
    return this.http.put(this.url + 'update-user?userName=' + oldName + '&newUserName=' + newName + '&userEmail=' + newEmail, this.options);
  }

  addGroupToUser(userName: string, groupName: string): Observable<any> {
    console.log(userName + ", " + groupName);
    return this.http.post(this.url + 'add-grouping-to-user?userName=' + userName + '&groupingName=' + groupName, this.options);
  }

  getGroupingsForUser(user: string): Observable<any[]> {
    return this.http.get(this.url + 'grouplistforuser?userName=' + user)
                    .map(data => data.json());
  }

}
