import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { UsersService } from './services/users.service';
import { MaterialModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { PdfsComponent } from './components/pdfs/pdfs.component';
import { UsersComponent } from './components/users/users.component';
import { SendComponent } from './components/send/send.component';
import { GroupsComponent } from './components/groups/groups.component';
import { UsersgroupsComponent } from './components/usersgroups/usersgroups.component';

const appRoutes: Routes = [
  {path:'', component: UsersgroupsComponent},
  {path:'pdfs', component: PdfsComponent},
  {path:'send', component: SendComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    PdfsComponent,
    UsersComponent,
    SendComponent,
    GroupsComponent,
    UsersgroupsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterialModule,
    FlexLayoutModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
