import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})
export class GroupsComponent implements OnInit {

  groups: IGroup[];
  showUsers: boolean;
  showForm: boolean;
  usersForGroup: IUser[];

  constructor(private usersService:UsersService) { }

  ngOnInit() {
    this.usersService.getGroups().subscribe(groups =>  {
      this.groups = groups;
      this.groups.forEach((item) => {
        item.groupingName = item.groupingName.charAt(0).toUpperCase() + item.groupingName.slice(1);
      });
    });
    this.showUsers = false;
    this.showForm = false;
  }

  showUsersForGroup(i) {
    this.usersService.getUsersForGrouping(this.groups[i].groupingName).subscribe(usersForGroup => this.usersForGroup = usersForGroup);
    this.showUsers = true;

  }

  toggleForm() {
    if(this.showForm == false) {
      this.showForm = true;
    } else {
      this.showForm = false;
    }
  }

  addGroup(groupName: string) {
    let group1 = {
        groupingId: 1,
        groupingName: groupName
      }
      this.groups.push(group1);
    this.usersService.addGroup(groupName).subscribe(groups => this.groups = groups);
    this.showForm = false;
  }

  deleteGroup(i) {
      this.usersService.deleteGroup(this.groups[i].groupingName).subscribe(groups => this.groups = groups);
      this.groups.splice(i, 1);
  }

}

interface IGroup {
  groupingId: number,
  groupingName: string
}

interface IUser {
  userId: number,
  userName: string,
  userEmail: string,
  userBirthday: string
}
