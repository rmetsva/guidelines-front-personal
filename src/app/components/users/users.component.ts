import { Component, OnInit, trigger, state, style, transition, animate, keyframes } from '@angular/core';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],

  animations: [
        trigger('fadeForm', [
            state('inactive', style({
                transform: 'translate(-500px, 0px)'
            })),
            state('active', style({
                transform: 'translate(0px, 0px)'
            })),
            transition('inactive => active', animate('400ms ease-in')),
            transition('active => inactive', animate('400ms ease-out'))
        ]),
    ]
})
export class UsersComponent implements OnInit {

  users: IUser[];
  showForm: boolean;
  showEditForm: boolean;
  userToEdit: IUser;
  errorMessage: string;
  groups: IGroup[];
  groupsForUser: IGroup[];
  state: string;

  constructor(private usersService:UsersService) {
   }

  ngOnInit() {
    this.usersService.getUsers().subscribe(users => this.users = users);
    this.usersService.getGroups().subscribe(groups => this.groups = groups);
    this.showForm = false;
    this.showEditForm = false;
    this.state = 'inactive';
  }

  deleteUser(i) {
    this.usersService.deleteUser(this.users[i].userName).subscribe(users => this.users = users);
    this.users.splice(i, 1);
    this.showEditForm = false;
  }

  toggleForm() {
    this.state = (this.state === 'inactive' ? 'active' : 'inactive');
  }

  addUser(name:string, email:string, birthday:string) {
      let user1 = {
        userId: 1,
        userName: name,
        userEmail: email,
        userBirthday: birthday
      }
      this.users.push(user1);
      this.usersService.addUser(name, email, birthday).subscribe(users => this.users = users);
      this.state = 'inactive';
  }

  showEditUserForm(i) {
    if (this.showEditForm == false) {
      this.showEditForm = true;
    } else if (this.showEditForm == true && this.userToEdit.userId == this.users[i].userId) {
      this.showEditForm = false;
    }
    this.userToEdit =  this.users[i];
    this.usersService.getGroupingsForUser(this.users[i].userName).subscribe(groupsForUser => this.groupsForUser = groupsForUser);
  }

  editUser(name:string, email:string) {
    this.usersService.editUser(this.userToEdit.userName, name, email).subscribe(users => this.users = users);
    let user1 = this.users.find(item => {
      return item.userName == this.userToEdit.userName;
    });
    user1.userName = name;
    user1.userEmail = email;
    this.showEditForm = false;
  }

  addGroupToUser(groupName:string) {
      this.usersService.addGroupToUser(this.userToEdit.userName, groupName).subscribe(users => this.users = users);
      this.groups.forEach(group => {
        if (group.groupingName == groupName) {
            this.groupsForUser.push(group);
        }
      });
  }

}

interface IUser {
  userId: number,
  userName: string,
  userEmail: string,
  userBirthday: string
}

interface IGroup {
  groupingId: number,
  groupingName: string
}
