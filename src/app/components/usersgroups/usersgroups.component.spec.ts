import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersgroupsComponent } from './usersgroups.component';

describe('UsersgroupsComponent', () => {
  let component: UsersgroupsComponent;
  let fixture: ComponentFixture<UsersgroupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersgroupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersgroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
